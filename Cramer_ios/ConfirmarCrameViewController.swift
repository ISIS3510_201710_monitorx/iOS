//
//  ConfirmarCrameViewController.swift
//  Cramer_ios
//
//  Created by Samuel Salazar on 13/03/17.
//  Copyright © 2017 Cramer Team. All rights reserved.
//

import Foundation
import UIKit
import AFNetworking
import MBProgressHUD
import Contacts
import ContactsUI
import MessageUI

class  ConfirmarCrameViewController: UIViewController, CNContactPickerDelegate, MFMessageComposeViewControllerDelegate {
    
    let contactPicker: CNContactPickerViewController = CNContactPickerViewController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        contactPicker.delegate = self

    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        print(contact)
        picker.dismiss(animated: true) {
            self.sendSMS(contact: contact.phoneNumbers[0].value.stringValue)
        }
    }
    

    @IBAction func ActionContactar(_ sender: Any) {
        
        let phone = "3214911936"
        print("Contactando a cramer")
        
        // 1
        let optionMenu = UIAlertController(title: nil, message: "Contactar Cramer", preferredStyle: .actionSheet)
        
        // 2
        let callAction = UIAlertAction(title: "Llamar", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Llamar")
            self.callNumber(phoneNumber: phone)
        })
        let smsAction = UIAlertAction(title: "SMS", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("SMS")
            self.sendSMS(contact: phone)
        })
        let waAction = UIAlertAction(title: "WhatsApp", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("WhatsApp")
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelar")
            
        })
        
        
        // 4
        optionMenu.addAction(callAction)
        optionMenu.addAction(smsAction)
        optionMenu.addAction(waAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
        
        
    }


    
    private func callNumber(phoneNumber:String) {
        if let url = URL(string:"tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    func sendSMS(contact: String) {
        if !MFMessageComposeViewController.canSendText() {
            print("No es posible enviar mensajes desde este dispositivo")
            return
        }

        let recipients: [String] = [contact]
        let messageController: MFMessageComposeViewController = MFMessageComposeViewController()
        messageController.messageComposeDelegate = self
        messageController.recipients = recipients
        messageController.body = "Mensaje de prueba"
        self.present(messageController, animated: true, completion: nil)

    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch result {
        case .sent:
            print("Message Sent")
            controller.dismiss(animated: true, completion: nil)
        case .failed:
            print("Message Failed")
            controller.dismiss(animated: true, completion: nil)
        case .cancelled:
            print("Message Cancelled")
            controller.dismiss(animated: true, completion: nil)
        }
    }
    
}

