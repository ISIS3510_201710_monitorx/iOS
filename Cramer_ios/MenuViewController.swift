//
//  MenuViewController.swift
//  Cramer_ios
//
//  Created by Daniel Soto on 3/12/17.
//  Copyright © 2017 Cramer Team. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var menuTable: UITableView!
    
    var ui_configured:Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeMode(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if (K.CramerMode.estudiante == true){
            //Cambiar a Cramer
            let cramerHome = storyboard.instantiateViewController(withIdentifier: "CramerHome")
            self.revealViewController().setFront(cramerHome, animated: true)
            
            K.CramerMode.estudiante = false
        }
        else{
            //Cambiar a Estudiante
            let estudianteHome = storyboard.instantiateViewController(withIdentifier: "EstudianteHome")
            self.revealViewController().setFront(estudianteHome, animated: true)
            
            K.CramerMode.estudiante = true
        }
        
        self.revealViewController().revealToggle(animated: true);
            
        {self.menuTable.reloadData()} ~> {};
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Main", for: indexPath)
            let modeB = (cell.viewWithTag(20) as! UIButton)
            
            if (K.CramerMode.estudiante == true){
                
            }
            else{
                (cell.viewWithTag(13) as! UILabel).text = "¿Necesitas aprender algo?"
                modeB.setTitle("Cambiar a modo Estudiante",for: .normal)
            }
            
            if (ui_configured == false){
                modeB.addNormalShadow()
                modeB.roundCorners(radius: 5)
                ui_configured = true
                
            }
            
            
            
            return cell
        default:
            return UITableViewCell()
        }
        
        
        
    }
}
