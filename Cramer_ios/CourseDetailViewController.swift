//
//  CourseDetailViewController.swift
//  Cramer_ios
//
//  Created by Samuel Salazar on 19/02/17.
//  Copyright © 2017 Cramer Team. All rights reserved.
//

import UIKit
import AFNetworking
import MBProgressHUD

class CourseDetailViewController: UIViewController {

    var course: Course!
    
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBOutlet weak var userTxt: UITextField!
    
    
    @IBOutlet weak var placeTxt: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        navBar.topItem?.title = course.name
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func askForACramer(_ sender: Any) {
        
        if userTxt.text == nil || userTxt.text == "" { showAlert(title: "Empty User", message: "You must provide a user name", closeButtonTitle: "Close"); return
        }
        
        if placeTxt.text == nil || placeTxt.text == "" { showAlert(title: "Empty Place", message: "You must provide a place", closeButtonTitle: "Close"); return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true);
        
        let params: [String:Any] = [
            "user":userTxt.text!,
            "place": placeTxt.text!,
            "course": course.id!
        ]
        
        print("Sending request with params: \(params)")
    
        
        manager.requestSerializer = AFJSONRequestSerializer()
        
        manager.post("meetings/", parameters: params, progress: { (progress) in
        }, success: { (task: URLSessionDataTask, response) in
            let dicResp: NSDictionary = response! as! NSDictionary
            print(dicResp)
            
            let alertCtrl = UIAlertController(title: "Succesfull Request",message: "Success", preferredStyle: .alert)
            
            let volverAction = UIAlertAction(title: "Back to courses", style: .default) {(action: UIAlertAction) in self.dismiss(animated: true, completion: nil)}
            
            alertCtrl.addAction(volverAction)
            
            MBProgressHUD.hide(for: self.view, animated: true);
            
            self.present(alertCtrl, animated: true) {}
            
            
        }) { (task: URLSessionDataTask?, error: Error) in
            print("Error Task: \(task) -- Error Response: \(error) ")
            self.showAlert(title: "Error en la solicitud", message: error.localizedDescription, closeButtonTitle: "Cerrar")
        }
    }
    
}
