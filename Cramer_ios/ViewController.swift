//
//  ViewController.swift
//  Cramer_ios
//
//  Created by Samuel Salazar on 19/02/17.
//  Copyright © 2017 Cramer Team. All rights reserved.
//

import UIKit
import AFNetworking
import MBProgressHUD

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    
     var courses:[Course] = [Course]()
    
    @IBOutlet weak var coursesTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.setStatusBarBackgroundColor(color: UIColor(netHex:0x03a9f4));
        
        // Do any additional setup after loading the view, typically from a nib.
        
        coursesTableView.delegate = self
        coursesTableView.dataSource = self
        
        MBProgressHUD.showAdded(to: self.view, animated: true);
        
        {self.getCourses()} ~> {}
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        MBProgressHUD.showAdded(to: self.view, animated: true);
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToCourseDetail"{
            let controladorDestino = segue.destination as! CourseDetailViewController
            controladorDestino.course = sender as! Course
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "courseCell")!
        
        let course: Course = courses[indexPath.row]
        
        let imageView: UIImageView = cell.viewWithTag(1) as! UIImageView
        
        imageView.setImageWith(URL(string: course.image!)!)
        
        let lblName: UILabel = cell.viewWithTag(2) as! UILabel
        lblName.text = course.name!
        
        let lblPrice: UILabel = cell.viewWithTag(3) as! UILabel
        lblPrice.text = String(course.price!)
        
        print("Actualizando celda: \(course.name)")
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "GoToCourseDetail", sender: courses[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    

    
    func getCourses() {
        manager.get("courses", parameters: nil, progress: { (progress) in
        }, success: { (task: URLSessionDataTask, response) in
            let arrayResponse: NSArray = response! as! NSArray

            for item in arrayResponse {
                let act: Course = Course(item as! Dictionary<String, AnyObject>)
                print("Course added: \(act)")
                self.courses.append(act)
                self.coursesTableView.reloadData()
            }
            
            MBProgressHUD.hide(for: self.view, animated: true);
        }) { (task: URLSessionDataTask?, error: Error) in
            print("Error Task: \(task) -- Error Response: \(error) ")
        }
    }
}

