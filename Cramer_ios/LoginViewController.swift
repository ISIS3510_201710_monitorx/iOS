//
//  LoginViewController.swift
//  Cramer_ios
//
//  Created by Daniel Soto on 3/11/17.
//  Copyright © 2017 Cramer Team. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var loginBox: UIView!
    @IBOutlet weak var fb: UIButton!
    @IBOutlet weak var google: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        loginBox.addNormalShadow()
        fb.addNormalShadow()
        google.addNormalShadow()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let controladorDestino = segue.destination as! RegisterViewController
        controladorDestino.parentview = self
        
        if segue.identifier == "Login"{
            controladorDestino.register = false
            
        }
        
        if segue.identifier == "Register"{
            controladorDestino.register = true
            
        }
    }

    @IBAction func loginWithFB(_ sender: Any) {
        
    }
    @IBAction func loginWithGoogle(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
