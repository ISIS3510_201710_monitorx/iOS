//
//  UniversityViewController.swift
//  Cramer_ios
//
//  Created by Daniel Soto on 3/11/17.
//  Copyright © 2017 Cramer Team. All rights reserved.
//

import UIKit

class UniversityViewController: UIViewController {

    @IBOutlet weak var uniandes: UIButton!
    
    var parentview:RegisterViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        uniandes.bordered(color: UIColor(netHex:0x858484))
    }

    @IBAction func choose(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        parentview.dismiss(self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
