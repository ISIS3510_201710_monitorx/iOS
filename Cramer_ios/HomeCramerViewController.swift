//
//  HomeCramerViewController.swift
//  Cramer_ios
//
//  Created by Daniel Soto on 3/13/17.
//  Copyright © 2017 Cramer Team. All rights reserved.
//

import UIKit
import SWRevealViewController

class HomeCramerViewController: UIViewController {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let revealViewController = self.revealViewController()
        if (revealViewController != nil){
            self.view.addGestureRecognizer((revealViewController?.panGestureRecognizer())!)
            self.menuButton.target = revealViewController!
            self.menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
