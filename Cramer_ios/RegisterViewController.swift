//
//  RegisterViewController.swift
//  Cramer_ios
//
//  Created by Daniel Soto on 3/11/17.
//  Copyright © 2017 Cramer Team. All rights reserved.
//

import UIKit
import MBProgressHUD
import Firebase

class RegisterViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var registerBox: UIView!
    @IBOutlet weak var nextB: UIButton!
    @IBOutlet weak var dismissB: UIButton!
    @IBOutlet weak var nombreText: UITextField!
    @IBOutlet weak var correoText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    var parentview:LoginViewController!
 
    var register:Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if (register == false){
            nombreText.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        registerBox.addNormalShadow()
        nextB.addNormalShadow()
        nextB.roundCorners(radius: 5)
        dismissB.addNormalShadow()
        dismissB.roundCorners(radius: 5)
    }

    @IBAction func nextStep(_ sender: Any) {
        MBProgressHUD.showAdded(to: registerBox, animated: true)
        
        self.nombreText.resignFirstResponder()
        self.correoText.resignFirstResponder()
        self.passwordText.resignFirstResponder()
        
        
        guard correoText.text != nil && correoText.text! != "" else{
            MBProgressHUD.hide(for: registerBox, animated: true)
            self.showAlert(title: "Lo sentimos", message: "Debes ingresar una dirección de correo.", closeButtonTitle: "Ok")
            return
        }
        let correo = correoText.text!
        
        guard passwordText.text != nil && passwordText.text! != ""  else{
            MBProgressHUD.hide(for: registerBox, animated: true)
            self.showAlert(title: "Lo sentimos", message: "Debes ingresar una contraseña.", closeButtonTitle: "Ok")
            return
        }
        let clave = passwordText.text!
        
        if (register == true){
            //register
            guard nombreText.text != nil && nombreText.text! != ""  else{
                MBProgressHUD.hide(for: registerBox, animated: true)
                self.showAlert(title: "Lo sentimos", message: "Debes ingresar tu nombre.", closeButtonTitle: "Ok")
                return
            }
            let nombre = nombreText.text!
            
            FIRAuth.auth()?.createUser(withEmail: correo, password: clave) { (user, error) in
                MBProgressHUD.hide(for: self.registerBox, animated: true)
                print(user!)
                if (error != nil){
                    self.showAlert(title: "Lo sentimos", message: "Ha ocurrido un error inesperado. Intenta de nuevo más tarde.", closeButtonTitle: "Ok")
                }
                else{
                    if (user != nil){
                        let req = user!.profileChangeRequest()
                        req.displayName = nombre
                        req.commitChanges(completion: nil)
                    }
                    
                    self.performSegue(withIdentifier: "Next", sender: nil)
                }
            }
        }else{
            //Login
            FIRAuth.auth()?.signIn(withEmail: correo, password: clave) { (user, error) in
                MBProgressHUD.hide(for: self.registerBox, animated: true)
                if (user != nil){
                    self.dismiss(self)
                }else{
                    self.showAlert(title: "Lo sentimos", message: "El usuario/contraseña ingresados no son correctos. Intenta de nuevo.", closeButtonTitle: "Ok")
                }
                
            }
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        (segue.destination as! UniversityViewController).parentview = self
    }

    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            self.parentview.dismiss(animated: true, completion: nil)
        })
        

        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //TextField Handler
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let tag = textField.tag
        
        switch tag {
        case 1:
            //Nombre
            textField.endEditing(true)
            self.view.viewWithTag(2)?.becomeFirstResponder()
        case 2:
            //Correo
            textField.endEditing(true)
            self.view.viewWithTag(3)?.becomeFirstResponder()
        case 3:
            //Clave
            textField.endEditing(true)
        default:
            return false
            //Ni idea
            
        }
        
        return true
    }

}
