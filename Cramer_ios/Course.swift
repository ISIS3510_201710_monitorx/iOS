//
//  Course.swift
//  Cramer
//
//  Created by Samuel Salazar on 19/02/17.
//  Copyright © 2017 Cramer Team. All rights reserved.
//

import UIKit

class Course: NSObject {
    
    var id: Int!
    var name: String!
    var price: Double?
    var image: String?
    
    override init() {
        super.init()
    }
    
    convenience init(_ dictionary: Dictionary<String, AnyObject>) {
        self.init()
        id = dictionary["id"] as! Int
        name = dictionary["name"] as! String
        price = dictionary["price"] as? Double
        image = dictionary["image"] as? String
    }
}
