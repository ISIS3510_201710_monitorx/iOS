//
//  HomeEstudianteViewController.swift
//  Cramer_ios
//
//  Created by Daniel Soto on 3/11/17.
//  Copyright © 2017 Cramer Team. All rights reserved.
//

import UIKit
import SWRevealViewController
import Firebase
import MBProgressHUD
import CoreLocation

class HomeEstudianteViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var card1: UIView!
    @IBOutlet weak var card2: UIView!
    
    @IBOutlet weak var express: UIButton!
    @IBOutlet weak var programada: UIButton!
    
    var locationManager: CLLocationManager = CLLocationManager()
    var userLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
//        try! FIRAuth.auth()!.signOut()
        // Do any additional setup after loading the view.
        
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                // User is signed in.
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.setStatusBarBackgroundColor(color: UIColor(netHex:0x03a9f4));
                let revealViewController = self.revealViewController()
                if (revealViewController != nil){
                    self.view.addGestureRecognizer((revealViewController?.panGestureRecognizer())!)
                    self.menuButton.target = revealViewController!
                    self.menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
                    revealViewController!.rearViewRevealWidth = 300
                    revealViewController!.rearViewRevealDisplacement = 50
                    revealViewController!.frontViewShadowOpacity = 0.15
                    revealViewController!.frontViewShadowOffset = CGSize(width: -2.0, height: 0)
                }
            } else {
                // No user is signed in.
                self.performSegue(withIdentifier: "Login", sender: nil)
            }
            
            self.card1.addNormalShadow()
            self.card2.addNormalShadow()
            self.express.addLightShadow()
            self.express.roundCorners(radius: 5)
            self.programada.addLightShadow()
            self.programada.roundCorners(radius: 5)
            
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            self.locationManager.delegate = self
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.startUpdatingLocation()
            self.userLocation = nil
            
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let latestLocation: CLLocation = locations[locations.count - 1]
        
        if userLocation == nil {
            userLocation = latestLocation
            self.locationManager.stopUpdatingLocation()
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error) {
        print("Location Manager Error: " + error.localizedDescription)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
